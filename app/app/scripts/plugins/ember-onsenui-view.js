window.Eons = {};

Eons.BaseView = Ember.View.extend({

});

Eons.ScreenView = Eons.BaseView.extend({
  classNames: 'screen-page',
});

Eons.SplitView = Eons.BaseView.extend({
	classNames: 'split-view',
});

Eons.SplitSecondaryView = Eons.BaseView.extend({
	classNames: 'split-secondary-view',
});

Eons.SplitMainView = Eons.BaseView.extend({
	classNames: 'split-main-view',
});

Eons.PageView = Eons.BaseView.extend({
  classNames: ['page', 'center'],
});
