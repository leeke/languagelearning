var App = window.App = Ember.Application.create();

/* Order and include as you please. */

require('scripts/plugins/*');

/*sketchpad*/
require('scripts/plugins/sketchpad/jquery-migrate-1.2.1');
require('scripts/plugins/sketchpad/raphael');
require('scripts/plugins/sketchpad/json2');
require('scripts/plugins/sketchpad/raphael.sketchpad');

/*ember app*/
require('scripts/controllers/*');
require('scripts/store');
require('scripts/models/*');
require('scripts/routes/*');
require('scripts/views/*');
require('scripts/router');
