App.WhiteboardView = Eons.PageView.extend({
  whiteboard: 1,
  didInsertElement: function() {
    var whiteboard = Raphael.sketchpad("whiteboard", {
      width: '100%',
      height: '100%',
      editing: true
    });
    var paper = whiteboard.paper();
    var container = whiteboard.container();
    var pen = whiteboard.pen();
    pen.width(1);
    this.set('whiteboard', whiteboard);
  }
});


App.WhiteboardMenuView = Eons.BaseView.extend({
  click: function(e) {
    var whiteboard = this.get('parentView.whiteboard');
    var target = e.target;
    var name = $(target).attr('name');

    whiteboard[name]();
  }
});
